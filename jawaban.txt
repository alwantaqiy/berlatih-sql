1.buat Database

 create database myshop; 

2.
table users
create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
table categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );
table items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(50),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );

3.
insert users
insert into users(name, email, password) values ("John Doe", "john@doe.com", "john123");
insert into users(name, email, password) values ("Jane Doe", "jane@doe.com", "jenita123");
insert categories
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
insert items
MariaDB [myshop]> insert into items(name, description, price, stock, category_id) values ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1);
Query OK, 1 row affected (0.106 sec)

MariaDB [myshop]> insert into items(name, description, price, stock, category_id) values ("Uniklooh", "baju keren dari brand ternama", "500000", "50", 2);
Query OK, 1 row affected (0.099 sec)

MariaDB [myshop]> insert into items(name, description, price, stock, category_id) values ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1);
Query OK, 1 row affected (0.102 sec)

4.
a. MariaDB [myshop]> select id, name, email from users;
b. MariaDB [myshop]> select * from items where price>1000000;
MariaDB [myshop]> select * from items where name like '%sang%';
c. MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

5.
MariaDB [myshop]> update items set price=2500000 where id=7;

catatan: id dari table items menjadi 7, 8, 9 Karena kesalahan sebelumnya.